;;; conan-integration.el --- Easily configure cmake projects and run/debug targets -*- lexical-binding: t -*-

;; Author: Gregor Muehleder <gregor.muehleder@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "28.1") project f s json eshell)
;; Homepage: https://gitlab.com/gregor.muehleder/conan-integration
;; Keywords: c c++ conan cmake languages tools
;; URL: https://gitlab.com/gregor.muehleder/conan-integration


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Provide functions to configure cmake projects, and to compile and
;; run a given target. Completions are provided with targets obtained
;; from cmake. It also support cmake presets.

;;; Code:
(require 'project)
(require 'f)
(require 's)
(require 'json)
(require 'cl-extra)
(require 'eshell)
(require 'tramp)

;; ---------------- custom variables ----------------

(defgroup conan-integration nil "Easily call cmake configure and run compiled targets." :group 'tools :prefix "conan-integration-")

(defcustom conan-integration-cmake-default-build-dir "build"
  "The build folder to use when no presets are used.

If this is nil, then using presets is required." :type '(string) :group 'conan-integration)

(defcustom conan-integration-cmake-default-generator nil
  "The generator to pass to cmake when no presets are used.

If this is nil, then the generator is not explicitly set." :type '(string) :group 'conan-integration)

(defcustom conan-integration-cmake-include-subproject-targets-during-completion t
  "If t, include subproject targets when presenting target names for completion.

When t (default) all targets are included during completion of
target names. If nil, then only targets from the main cmake
project are included (targets with projectIndex equal to zero)."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-cmake-hide-utility-targets-during-completion nil
  "If t, then utility targets are not included during completion."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-cmake-hide-library-targets-during-completion nil
  "If t, then library targets are not included during completion."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-create-compile-commands-link t
  "If t, make a link of `compile_commands.json' to the project root.

This helps lsp and clangd correctly parsing the project files."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-custom-debug-function nil
  "Custom Debugging Function.

This user-defined function is invoked when the debugging
should start and should accept two arguments: the executable
path and the working directory."
  :type 'function :group 'conan-integration)

(defcustom conan-integration-additional-install-arguments "-u --build=missing" "Extra arguments to pass to conan." :type '(string) :group 'conan-integration)

(defcustom conan-integration-additional-build-arguments "" "Extra arguments to pass to conan." :type '(string) :group 'conan-integration)

(defcustom conan-integration-include-conan-toolchain-file nil
  "If t, pass '--toolchain conan_toolchain.cmake' to cmake.

If you are using the 'CMakeToolchain' generator, set this to true
in a directory local variable in your project."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-eshell-name "conan-integration-eshell"
  "Default eshell buffer name." :type '(string) :group 'conan-integration)

(defcustom conan-integration-build-folder "${workspaceFolder}/build/${profileName}"
  "Location of the folder where conan builds the package.

${workspaceFolder} gets replaced by the projectile-project-root.
${profileName} get replaced by the host profile name." :type '(string) :group 'conan-integration)

(defcustom conan-integration-use-dap-cpptools t
  "Create a debug template for dap-cpptools."
  :type 'boolean :safe #'booleanp :group 'conan-integration)

(defcustom conan-integration-dap-cpptools-template
  (list :type "cppdbg"
        :request "launch"
        :MIMode "lldb"
        :name "conan-integration"
        :cwd "${workspaceFolder}")
  "The default dap-cpptools template to use."
  :type 'sexp :group 'conan-integration)

;; ---------------- other variables ----------------

(defvar conan-integration-eshell-last-name conan-integration-eshell-name)
(defvar conan-integration-eshell-sentinel-commands nil)
(defvar conan-integration-current-preset nil)
(defvar conan-integration-current-target nil)
(defvar conan-integration-current-build-profile nil)
(defvar conan-integration-current-host-profile nil)

;; ---------------- variable helpers ----------------

(defun conan-integration-get-var-for-project (var)
  "Get the current preset VAR."
  (let ((curCell (assoc (conan-integration-get-project-root-folder) (eval var))))
   (if curCell
    (cdr curCell)
    nil)))

(defun conan-integration-set-var-for-project (var value)
  "Set the current VAR to VALUE."
  (let ((curCell (assoc (conan-integration-get-project-root-folder) (eval var))))
    (if curCell
      (setcdr curCell value)
      (set var (append (eval var) `((,(conan-integration-get-project-root-folder) . ,value)))))))

;; ---------------- other functions ----------------

(defun conan-integration-add-or-replace-key-value (key value cur-list)
  "Add or replace the KEY with the VALUE in CUR-LIST."
  (let* ((new-list (copy-sequence cur-list))
         (key-pos (position key new-list)))
    (if key-pos
      (setf (nth (1+ key-pos) new-list) value)
      (setq new-list (append new-list (list key value))))
    new-list))

(defun conan-integration-remove-key-value (key cur-list)
  "Remove KEY and its associated value from CUR-LIST if present."
  (let ((index (position key cur-list)))
    (if index
      (let ((new-list (copy-sequence cur-list)))
        (setq new-list (delete (nth (+ index 1) new-list) new-list))
        (setq new-list (delete key new-list))
        new-list)
      cur-list)))

(defun conan-integration-get-value-at-key (key cur-list)
  "Get value with KEY from CUR-LIST if present."
  (let ((index (position key cur-list)))
    (if index
      (nth (+ index 1) cur-list)
      nil)))

;; ---------------- folder functions ----------------

(defun conan-integration-get-project-root-folder ()
  "Get the current project root using Emacs built-in project."
  (expand-file-name (project-root (project-current))))

(defun conan-integration-get-cwd-for-path (path)
  "Get cwd for PATH."
  (if (and (eq system-type 'windows-nt) (string-match "^[A-Za-z]:" path))
      (format "%s && cd \"%s\"" (substring path 0 2) path)
      (format "cd \"%s\"" path)))

(defun conan-integration-get-local-folder (folder)
  "Get the local folder from FOLDER."
  (if (tramp-tramp-file-p folder)
    (tramp-file-name-localname (tramp-dissect-file-name folder))
    folder))

(defun conan-integration-get-remote-folder (tramp-folder folder)
  "Get the remote folder from FOLDER that is in the same remote as TRAMP-FOLDER."
  (if (file-name-absolute-p folder)
    (if (and (tramp-tramp-file-p tramp-folder) (not (tramp-tramp-file-p folder)))
        (let* ((local-folder (conan-integration-get-local-folder tramp-folder))
               (tramp-part (substring tramp-folder 0 (- (length tramp-folder) (length local-folder)))))
          (format "%s%s" tramp-part folder))
      folder)
    (expand-file-name folder (file-name-directory tramp-folder))))

(defun conan-integration-file-exists-p (file)
  "Check if FILE really exists by disabling caching."
  (let ((tmp remote-file-name-inhibit-cache))
    (setq remote-file-name-inhibit-cache t)
    (let ((ret (file-exists-p file)))
      (setq remote-file-name-inhibit-cache tmp)
      ret)))

;; ---------------- eshell functions ----------------

(defun conan-integration-eshell-create-new-window ()
  "Create new eshell window."
  (let ((eshellBufferName (generate-new-buffer-name conan-integration-eshell-name)))
    (split-window-below)
    (other-window 1)
    (eshell)
    (rename-buffer eshellBufferName)
    (compilation-shell-minor-mode)
    (other-window -1)
    (setq conan-integration-eshell-last-name eshellBufferName)))

(defun conan-integration-eshell-open-last-window ()
  "Open last eshell window."
  (let ((eshell-window (get-buffer-window conan-integration-eshell-last-name)))
    (unless eshell-window
      (split-window-below)
      (other-window 1)
      (switch-to-buffer conan-integration-eshell-last-name)
      (other-window -1))))

(defun conan-integration-eshell-open-window ()
  "Open the last or a new eshell window."
  (if (get-buffer conan-integration-eshell-last-name)
    (if (get-buffer-process conan-integration-eshell-last-name)
      (conan-integration-eshell-create-new-window)
      (conan-integration-eshell-open-last-window))
    (conan-integration-eshell-create-new-window)))

(defun conan-integration-eshell-run-command (command)
  "Run a eshell COMMAND."
  (let ((cur-window (selected-window)))
    (conan-integration-eshell-open-window)
    (let ((eshell-buffer (get-buffer conan-integration-eshell-last-name)))
      (pop-to-buffer eshell-buffer)
      (goto-char (point-max))
      (insert command)
      (eshell-send-input))
    (select-window cur-window)))

(defun conan-integration-eshell-run-function-after-program-finishes (callback)
  "Call a CALLBACK when eshell finishes."
  (let* ((bufferName conan-integration-eshell-last-name)
         (bufferProcess (get-buffer-process bufferName)))
    (if bufferProcess
      (setq conan-integration-eshell-sentinel-commands (append conan-integration-eshell-sentinel-commands (list `(,bufferProcess . ,callback))))
      (funcall callback))))

(defun conan-integration-eshell-sentinel (process event)
  "The eshell sentinel with PROCESS and EVENT."
  (when (string= event "finished\n")
    (let ((curElement (assoc process conan-integration-eshell-sentinel-commands)))
      (when curElement (funcall (cdr curElement)))
      (setq conan-integration-eshell-sentinel-commands (cl-remove curElement conan-integration-eshell-sentinel-commands)))))

(advice-add 'eshell-sentinel :after 'conan-integration-eshell-sentinel)

;; ---------------- dap-mode ----------------

(defun conan-integration-maybe-add-dap-template (program)
  "Add dap template when conan-integration-use-dap-cpptools is t.
Use conan-integration-dap-cpptools-template and replace :program with PROGRAM."
  (conan-integration-maybe-remove-dap-template)
  (when conan-integration-use-dap-cpptools
    (dap-register-debug-template
      (conan-integration-get-value-at-key
        :name conan-integration-dap-cpptools-template)
      (conan-integration-add-or-replace-key-value
        :program program conan-integration-dap-cpptools-template))))

(defun conan-integration-maybe-remove-dap-template ()
  "Remove dap template when conan-integration-use-dap-cpptools is t."
  (when conan-integration-use-dap-cpptools
    (cl-remove-if
      (lambda (item)
        (string=
          (car item)
          (conan-integration-get-value-at-key
            :name conan-integration-dap-cpptools-template)))
      dap-debug-template-configurations)))

;; ---------------- cmake codemodel ----------------

(defun conan-integration-get-cmake-query-folder (build-folder)
  "Get the query folder for the codemodel-v2 file in BUILD-FOLDER."
  (expand-file-name ".cmake/api/v1/query/client-emacs" build-folder))

(defun conan-integration-get-cmake-reply-folder (build-folder)
  "Get the reply folder for the codemodel-v2 file in BUILD-FOLDER."
  (expand-file-name ".cmake/api/v1/reply/" build-folder))

(defun conan-integration-get-path-of-codemodel-query-file (build-folder)
  "Get the full path of the codemodel-query-file in BUILD-FOLDER."
  (expand-file-name "codemodel-v2" (conan-integration-get-cmake-query-folder build-folder)))

(defun conan-integration-get-codemodel-reply-json-filename (build-folder)
  "Get the name of the json file with the targets in BUILD-FOLDER.

This file is created by CMake's File API."

  (nth 0 (f-glob "codemodel-v2*json" (conan-integration-get-cmake-reply-folder build-folder))))

(defun conan-integration-get-target-json-filename (build-folder target)
  "Get the json filename of the TARGET in BUILD-FOLDER."
  (expand-file-name (gethash "jsonFile" target) (conan-integration-get-cmake-reply-folder build-folder)))

(defun conan-integration-create-empty-codemodel-file (build-folder)
  "Create an empty codemodel query file for CMake's file API in BUILD-FOLDER."
  (let ((query-file (conan-integration-get-path-of-codemodel-query-file build-folder)))
    (unless (conan-integration-file-exists-p query-file)
      (make-empty-file query-file t))))

(defun conan-integration-get-cmake-targets-from-codemodel-json-file (json-filename &optional predicate)
  "Return the targets found in JSON-FILENAME that matches PREDICATE."
  (let* ((json-object-type 'hash-table)
         (json-array-type 'list)
         (json-key-type 'string)
         (data (json-read-file json-filename))
         (targets (make-hash-table)))
    (cl-loop for configuration in (gethash "configurations" data) do
      (cl-loop for target in (gethash "targets" configuration) do
        (when (or (not predicate) (funcall predicate target))
          (puthash (gethash "name" target) target targets))))
    targets))

(defun conan-integration-get-cmake-target-info (build-folder target)
  "Add additional info of TARGET in BUILD-FOLDER."
  (let* ((json-filename (expand-file-name (gethash "jsonFile" target) (conan-integration-get-cmake-reply-folder build-folder)))
         (json-object-type 'hash-table)
         (json-array-type 'list)
         (json-key-type 'string)
         (data (json-read-file json-filename)))
    (puthash "type" (gethash "type" data) target)
    (puthash "artifacts" (gethash "artifacts" data) target)))

(defun conan-integration-get-cmake-targets-from-codemodel (build-folder &optional predicate)
  "Get cmake targets from codemodel in BUILD-FOLDER that match PREDICATE."
  (let* ((json-file (conan-integration-get-codemodel-reply-json-filename build-folder))
         (targets (conan-integration-get-cmake-targets-from-codemodel-json-file json-file predicate)))
    (maphash (lambda (_ value) (conan-integration-get-cmake-target-info build-folder value)) targets)
    targets))

;; ---------------- cmake presets ----------------

(defun conan-integration-get-all-configure-presets-from-file (cmake-preset-file preset-table)
  "Get all presets from the CMAKE-PRESET-FILE recursively with PRESET-TABLE."
  (when (conan-integration-file-exists-p cmake-preset-file)
    (let* ((json-object-type 'hash-table)
           (json-array-type 'list)
           (json-key-type 'string)
           (data (json-read-file cmake-preset-file))
           (presets (gethash "configurePresets" data))
           (includes (gethash "include" data)))
      (cl-loop for preset in presets do
        (let ((binary-dir (gethash "binaryDir" preset)))
          (unless (gethash binary-dir preset-table)
            (puthash binary-dir preset preset-table))))

      (cl-loop for include in includes do
        (conan-integration-get-all-configure-presets-from-file (conan-integration-get-remote-folder cmake-preset-file include) preset-table)))))

(defun conan-integration-prompt-preset (&optional no-throw)
  "Search for presets and prompt the user to select one.

Conditionally throws when NO-THROW is nil."
  (let ((presets (make-hash-table)))
    (cl-loop for preset-file-name in '("CMakePresets.json" "CMakeUserPresets.json") do
      (let ((file (expand-file-name preset-file-name (conan-integration-get-project-root-folder))))
        (when (conan-integration-file-exists-p file)
          (conan-integration-get-all-configure-presets-from-file file presets))))
    (if (> (hash-table-count presets) 0)
        (let (preset-list)
          (maphash (lambda (_ value) (push `(,(gethash "name" value) ,value) preset-list)) presets)
          (push '("No Preset") preset-list)
          (assoc (completing-read "Select preset: " preset-list) preset-list))
      (unless no-throw (error "Tried to select preset but none found")))))

;;;###autoload
(defun conan-integration-cmake-select-preset (&optional no-throw)
  "Select a preset and store it. Conditionally throw when NO-THROW is nil."
  (interactive)
  (conan-integration-set-var-for-project 'conan-integration-current-preset (conan-integration-prompt-preset no-throw)))

(defun conan-integration-cmake-maybe-select-preset (&optional no-throw)
  "Select preset if currently there is no preset selected.
Conditionally throws when NO-THROW is nil."
  (unless (conan-integration-get-var-for-project 'conan-integration-current-preset)
    (conan-integration-cmake-select-preset no-throw)))

;; ---------------- cmake targets ----------------

(defun conan-integration-get-targets (&optional callback)
  "Get targets with the codemodel file. Configure cmake if the file does not exist.
Call CALLBACK with targets as argument."
  (conan-integration-cmake-maybe-select-preset t)

  (let ((after-configure
          (lambda ()
            (let ((targets (conan-integration-get-cmake-targets-from-codemodel (conan-integration-get-build-folder))))
              (when callback (funcall callback targets))))))
    (if (conan-integration-get-codemodel-reply-json-filename (conan-integration-get-build-folder))
      (funcall after-configure)
      (conan-integration-cmake-configure after-configure))))

(defun conan-integration-prompt-target (&optional callback no-throw)
  "Search for targets and prompt the user to select one.
Call CALLBACK after execution with the target as argument.
Conditionally throws when NO-THROW is nil."
  (let (target-list)
    (conan-integration-get-targets
      (lambda (targets)
        (if (> (hash-table-count targets) 0)
          (progn
            (maphash (lambda (key value) (push `(,key ,value) target-list)) targets)
            (push '("all" nil) target-list)
            (push '("clean" nil) target-list)
            (let ((target
                    (assoc (completing-read "Select target: " target-list) target-list)))
              (when callback (funcall callback target))))
          (unless no-throw (error "Tried to select target but none found")))))))

;;;###autoload
(defun conan-integration-cmake-select-target (&optional callback no-throw)
  "Select a target and store it.
Call CALLBACK after selection.
Conditionally throws when NO-THROW is nil."
  (interactive)
  (conan-integration-prompt-target
    (lambda (target)
      (conan-integration-set-var-for-project 'conan-integration-current-target target)
      (let* ((build-folder (conan-integration-get-build-folder))
             (executable (expand-file-name (conan-integration-get-executable-relative) build-folder)))
        (conan-integration-maybe-add-dap-template executable))
      (when callback (funcall callback)))
    no-throw))

(cl-defun conan-integration-cmake-maybe-select-target (&optional callback no-throw (no-throw-preset t))
  "Select target if currently there is no target selected.
Call CALLBACK after selection.
Conditionally throws on target selection when NO-THROW is nil.
Conditionally throws on preset selection when NO-THROW-PRESET is nil."
  (conan-integration-cmake-maybe-select-preset no-throw-preset)
  (if (conan-integration-get-var-for-project 'conan-integration-current-target)
    (when callback (funcall callback))
    (conan-integration-cmake-select-target callback no-throw)))

(defun conan-integration--target-is-in-projectIndex0-p (target)
  "Return t if the projectIndex field of TARGET is 0."
  (eq (alist-get 'projectIndex target) 0))

(defun conan-integration--target-is-not-utility-p (target)
  "Return t if TARGET type is not `UTILITY'."
  (not (equal (alist-get 'type target) "UTILITY")))

(defun conan-integration--target-is-not-library-p (target)
  "Return t if TARGET type is not `LIBRARY'."
  (let ((type (alist-get 'type target)))
    (when type
      (not (equal (car (cdr (split-string type "_"))) "LIBRARY")))))

;; ---------------- cmake run ----------------

(defun conan-integration-get-current-preset ()
  "Get the current cmake preset."
  (unless (or (not (conan-integration-get-var-for-project 'conan-integration-current-preset)) (string= (car (conan-integration-get-var-for-project 'conan-integration-current-preset)) "No Preset"))
    (nth 1 (conan-integration-get-var-for-project 'conan-integration-current-preset))))

(defun conan-integration-get-build-folder (&optional preset)
  "Get the current build folder for the PRESET."
  (let ((cur-preset (or preset (conan-integration-get-current-preset))))
    (if cur-preset
      (conan-integration-get-remote-folder (conan-integration-get-project-root-folder) (gethash "binaryDir" cur-preset))
      (expand-file-name "build" (conan-integration-get-project-root-folder)))))

(defun conan-integration-additional-config-args (preset)
  "Get additional config args for PRESET."
  (let ((cur-str ""))
    (when preset
      (let ((generator (gethash "generator" preset)))
        (when generator (setq cur-str (format "%s -G \"%s\"" cur-str generator))))
      (let ((tc (gethash "toolchainFile" preset)))
        (when tc (setq cur-str (format "%s -DCMAKE_TOOLCHAIN_FILE=\"%s\"" cur-str tc))))
      (let ((cache-vars (gethash "cacheVariables" preset)))
        (when cache-vars (maphash (lambda (key value) (setq cur-str (format "%s -D%s=%s" cur-str key value))) cache-vars))))
    cur-str))

(defun conan-integration-get-executable-relative (&optional target)
  "Get the relative build-folder for the TARGET."
  (let ((cur-target (or target (conan-integration-get-var-for-project 'conan-integration-current-target))))
    (unless cur-target
      (error "There is no executable for the current target"))
    (unless (string= (gethash "type" (nth 1 cur-target)) "EXECUTABLE")
      (error "The target is not an executable"))
    (gethash "path" (car (gethash "artifacts" (nth 1 cur-target))))))

;;;###autoload
(defun conan-integration-cmake-configure (&optional callback)
  "Configure cmake and run CALLBACK afterwards."
  (interactive)
  (conan-integration-cmake-maybe-select-preset t)
  (let* ((preset (conan-integration-get-current-preset))
         (root-folder (conan-integration-get-project-root-folder))
         (build-folder (conan-integration-get-build-folder preset))
         (args (conan-integration-additional-config-args preset)))
    (conan-integration-create-empty-codemodel-file build-folder)
    (conan-integration-eshell-run-command (format "cmake -B \"%s\" -S \"%s\" %s" (conan-integration-get-local-folder build-folder) (conan-integration-get-local-folder root-folder) args))
    (conan-integration-eshell-run-function-after-program-finishes
      (lambda ()
        (let ((compile-commands-file (expand-file-name "compile_commands.json" build-folder)))
          (when (conan-integration-file-exists-p compile-commands-file)
            (copy-file compile-commands-file (expand-file-name "compile_commands.json" root-folder) t)))
        (when callback (funcall callback))))))

;;;###autoload
(defun conan-integration-cmake-build (&optional callback)
  "Build cmake and call CALLBACK after."
  (interactive)
  (conan-integration-cmake-maybe-select-target
    (lambda ()
      (let* ((preset (conan-integration-get-current-preset))
             (target (conan-integration-get-var-for-project 'conan-integration-current-target))
             (build-folder (conan-integration-get-build-folder preset)))
        (conan-integration-eshell-run-command (format "cmake --build \"%s\" --target %s" (conan-integration-get-local-folder build-folder) (car target)))
        (conan-integration-eshell-run-function-after-program-finishes
          (lambda ()
            (when callback (funcall callback))))))))

;;;###autoload
(defun conan-integration-cmake-run (&optional callback)
  "Run program and call CALLBACK afterwards."
  (interactive)
  (conan-integration-cmake-maybe-select-target
    (lambda ()
      (let* ((root-folder (conan-integration-get-project-root-folder))
             (build-folder (conan-integration-get-build-folder))
             (executable (expand-file-name (conan-integration-get-executable-relative) build-folder))
             (cwd (conan-integration-get-cwd-for-path root-folder)))
        (conan-integration-eshell-run-command (format "%s && \"%s\"" cwd executable))
        (conan-integration-eshell-run-function-after-program-finishes
          (lambda ()
            (when callback (funcall callback))))))))

;; ---------------- conan packages ----------------

(defun conan-integration-prompt-profiles (&optional selectString)
  "Search for profiles and prompt the user to select one.

Use a custom SELECTSTRING for the prompt."
  (read-file-name (or selectString "Select profile: ") (projectile-project-root)))

;;;###autoload
(defun conan-integration-select-build-profile (&optional no-throw)
  "Select a build profile and conditionally throw with NO-THROW."
  (interactive)
  (let ((curProfile (conan-integration-prompt-profiles "Select build profile: ")))
    (when curProfile (conan-integration-set-var-for-project 'conan-integration-current-build-profile curProfile))))

(defun conan-integration-maybe-select-build-profile (&optional no-throw)
  "Maybe select a build profile and conditionally throw with NO-THROW."
  (unless (conan-integration-get-var-for-project 'conan-integration-current-build-profile)
    (conan-integration-select-build-profile no-throw)))

;;;###autoload
(defun conan-integration-select-host-profile (&optional no-throw)
  "Select a host profile and conditionally throw with NO-THROW."
  (interactive)
  (let ((curProfile (conan-integration-prompt-profiles "Select host profile: ")))
    (when curProfile (conan-integration-set-var-for-project 'conan-integration-current-host-profile curProfile))))

(defun conan-integration-maybe-select-host-profile (&optional callback no-throw)
  "Maybe select a host profile and conditionally throw with NO-THROW.
Execute CALLBACK after selection."
  (if (conan-integration-get-var-for-project 'conan-integration-current-host-profile)
    (when callback (funcall callback))
    (conan-integration-select-host-profile no-throw)))

;;;###autoload
(defun conan-integration-select-profile (&optional no-throw)
  "Select a profile and conditionally throw with NO-THROW."
  (interactive)
  (let ((curProfile (conan-integration-prompt-profiles)))
    (when curProfile
      (conan-integration-set-var-for-project 'conan-integration-current-build-profile curProfile)
      (conan-integration-set-var-for-project 'conan-integration-current-host-profile curProfile))))

(defun conan-integration-maybe-select-profile (&optional no-throw)
  "Maybe select a build profile and conditionally throw with NO-THROW."
  (when (or (not (conan-integration-get-var-for-project 'conan-integration-current-build-profile))
            (not (conan-integration-get-var-for-project 'conan-integration-current-host-profile))
            (not (string= (conan-integration-get-var-for-project 'conan-integration-current-build-profile) (conan-integration-get-var-for-project 'conan-integration-current-host-profile))))
    (conan-integration-select-profile no-throw)))

(defun conan-integration-replace-string-in-file (filename old-string new-string)
  "Replace all occurrences of OLD-STRING with NEW-STRING in FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (goto-char (point-min))
    (while (search-forward old-string nil t)
      (replace-match new-string))
    (write-region (point-min) (point-max) filename)))

(defun conan-integration-patch-cmake-preset (folder name)
  "Path the cmake preset file in FOLDER with the NAME."
  (let ((fileName (expand-file-name "CMakePresets.json" folder)))
    (when (conan-integration-file-exists-p fileName)
      (conan-integration-replace-string-in-file fileName "conan-release" name)
      (conan-integration-replace-string-in-file fileName "conan-debug" name))))

;;;###autoload
(defun conan-integration-install ()
  "Run conan install."
  (interactive)
  (conan-integration-maybe-select-build-profile)
  (conan-integration-maybe-select-host-profile)
  (let ((curFolder conan-integration-build-folder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${workspaceFolder}/") (projectile-project-root) curFolder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${workspaceFolder}") (projectile-project-root) curFolder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${profileName}") (file-name-nondirectory (conan-integration-get-var-for-project 'conan-integration-current-host-profile)) curFolder))
    (let ((fileName (expand-file-name "CMakePresets.json" curFolder)))
      (when (conan-integration-file-exists-p fileName)
        (delete-file fileName)))
    (conan-integration-eshell-run-command (format "conan install -pr:b \"%s\" -pr:h \"%s\" -of \"%s\" %s \"%s\"" (conan-integration-get-local-folder (conan-integration-get-var-for-project 'conan-integration-current-build-profile)) (conan-integration-get-local-folder (conan-integration-get-var-for-project 'conan-integration-current-host-profile)) (conan-integration-get-local-folder curFolder) conan-integration-additional-install-arguments (conan-integration-get-local-folder (projectile-project-root))))
    (conan-integration-eshell-run-function-after-program-finishes
      (lambda ()
        (conan-integration-patch-cmake-preset curFolder (file-name-nondirectory (conan-integration-get-var-for-project 'conan-integration-current-host-profile)))))))

;;;###autoload
(defun conan-integration-build ()
  "Run conan build."
  (interactive)
  (conan-integration-maybe-select-build-profile)
  (conan-integration-maybe-select-host-profile)
  (let ((curFolder conan-integration-build-folder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${workspaceFolder}/") (projectile-project-root) curFolder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${workspaceFolder}") (projectile-project-root) curFolder))
    (setq curFolder (replace-regexp-in-string (regexp-quote "${profileName}") (file-name-nondirectory (conan-integration-get-var-for-project 'conan-integration-current-host-profile)) curFolder))
    (conan-integration-eshell-run-command (format "conan build -pr:b \"%s\" -pr:h \"%s\" -of \"%s\" %s \"%s\"" (conan-integration-get-local-folder (conan-integration-get-var-for-project 'conan-integration-current-build-profile)) (conan-integration-get-local-folder (conan-integration-get-var-for-project 'conan-integration-current-host-profile)) (conan-integration-get-local-folder curFolder) conan-integration-additional-build-arguments (conan-integration-get-local-folder (projectile-project-root))))
    (conan-integration-eshell-run-function-after-program-finishes
      (lambda ()
        (conan-integration-patch-cmake-preset curFolder (file-name-nondirectory (conan-integration-get-var-for-project 'conan-integration-current-host-profile)))))))

(provide 'conan-integration)
;;; conan-integration.el ends here
